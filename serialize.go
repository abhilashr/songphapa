package main

import (
	"strconv"

	"gitlab.com/abhilashr/songphapa/model"
)

func Serialize(name string, amount string, ccnumber string, cvv string, month string, year string) (content model.Record, err error) {
	parsedAmount, err := strconv.ParseInt(amount, 10, 64)
	if err != nil {
		return content, err
	}
	parsedMonth, err := strconv.Atoi(month)
	if err != nil {
		return content, err
	}
	parsedYear, err := strconv.Atoi(year)
	if err != nil {
		return content, err
	}
	content = model.Record{Name: name, Amount: parsedAmount, Ccnumber: ccnumber, Cvv: cvv, Expmonth: parsedMonth, Expyear: parsedYear}
	return content, nil
}
