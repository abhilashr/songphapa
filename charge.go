package main

import (
	"time"

	"github.com/omise/omise-go"
	"github.com/omise/omise-go/operations"
	"gitlab.com/abhilashr/songphapa/model"
)

func charge(record model.Record, throttleTime int, config *Config) (status bool) {
	time.Sleep(time.Duration(throttleTime) * time.Second)
	client, e := omise.NewClient(config.Key.Public, config.Key.Secret)
	if e != nil {
		return status
	}

	token, createToken := &omise.Token{}, &operations.CreateToken{
		Name:            record.Name,
		Number:          record.Ccnumber,
		ExpirationMonth: time.Month(record.Expmonth),
		ExpirationYear:  record.Expyear,
		SecurityCode:    record.Cvv,
	}
	if err := client.Do(token, createToken); err != nil {
		return status
	}

	charge, createCharge := &omise.Charge{}, &operations.CreateCharge{
		Amount:   record.Amount,
		Currency: config.Application.Currency,
		Card:     token.ID,
	}

	if e := client.Do(charge, createCharge); e != nil {
		return status
	}

	status = charge.Paid
	return status
}
