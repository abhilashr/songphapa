FROM golang:1.10

# Set the Current Working Directory inside the container
WORKDIR $GOPATH/src/gitlab.com/abhilashr/songphapa

# Copy everything from the current directory to the PWD (Present Working Directory) inside the container
COPY . .

# Download all the dependencies
RUN go get -d -v ./...

# Install the package
RUN go install -v ./...

# RUN go run . ./data/fng.1000.csv.rot128

# Run the executable
CMD ["songphapa", "./data/fng.1000.csv.rot128"]
