package main

import (
	"fmt"

	"gitlab.com/abhilashr/songphapa/model"
)

func Summarize(record model.Record, context *model.SummaryContext, iteration int, status bool) {
	if record.Amount > context.Persons[0].Amount {

		assignPerson(&context.Persons[2], context.Persons[1])
		assignPerson(&context.Persons[1], context.Persons[0])
		context.Persons[0] = model.SummaryPersonContext{Name: record.Name, Amount: record.Amount}

	} else if record.Amount > context.Persons[1].Amount {

		assignPerson(&context.Persons[2], context.Persons[1])
		context.Persons[1] = model.SummaryPersonContext{Name: record.Name, Amount: record.Amount}

	} else if record.Amount > context.Persons[2].Amount {
		context.Persons[2] = model.SummaryPersonContext{Name: record.Name, Amount: record.Amount}
	}

	context.Average = incrementalAverage(context.Average, float64(record.Amount), iteration)
	if status {
		context.Success += record.Amount
	} else {
		context.Failure += record.Amount
	}
	context.Total += record.Amount
}

func assignPerson(first *model.SummaryPersonContext, second model.SummaryPersonContext) {
	first.Name = second.Name
	first.Amount = second.Amount
}

func incrementalAverage(currentAverage float64, newAmount float64, iteration int) float64 {
	return (currentAverage*float64(iteration) + newAmount) / (float64(iteration) + 1)
}

func printSummary(context *model.SummaryContext, currency string) {
	fmt.Println()
	fmt.Printf("%-30s: %s %-6d\n", "total received", currency, context.Total)
	fmt.Printf("%-30s: %s %-6d\n", "successfully donated", currency, context.Success)
	fmt.Printf("%-30s: %s %-6d\n\n", "faulty donation", currency, context.Failure)
	fmt.Printf("%-30s: %s %-6.2f\n", "average per person", currency, context.Average)

	fmt.Printf("%-30s: %-6s\n", "top donors", context.Persons[0].Name)
	fmt.Printf("%-30s: %-6s\n", "", context.Persons[1].Name)
	fmt.Printf("%-30s: %-6s\n", "", context.Persons[2].Name)
	fmt.Println()
}
