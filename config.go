package main

import (
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Application struct {
		Currency    string `yaml:"currency"`
		MaxThrottle string `yaml:"maxThrottle"`
	} `yaml:"application"`
	Key struct {
		Public string `yaml:"public"`
		Secret string `yaml:"secret"`
	} `yaml:"keys"`
}

func NewConfig(configPath string) (*Config, error) {
	config := &Config{}

	file, err := os.Open(configPath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	d := yaml.NewDecoder(file)

	if err := d.Decode(&config); err != nil {
		return nil, err
	}

	return config, nil
}
