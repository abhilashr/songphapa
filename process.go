package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strconv"

	"gitlab.com/abhilashr/songphapa/cipher"
	"gitlab.com/abhilashr/songphapa/model"
)

func process(path string, config *Config) error {

	if fileRef, err := os.Open(path); err != nil {
		return err
	} else {
		reader := bufio.NewReader(fileRef)
		decrypter, err := cipher.NewRot128Reader(reader)
		if err != nil {
			return err
		}

		parsedReader := csv.NewReader(decrypter)
		throttleTime := 0
		iteration := 0
		context := new(model.SummaryContext)
		context.Persons = make([]model.SummaryPersonContext, 3)
		maxThrottle, err := strconv.Atoi(config.Application.MaxThrottle)
		if err != nil {
			return err
		}

		fmt.Println("performing donations...")
		for {
			record, err := parsedReader.Read()
			if err == io.EOF {
				break
			} else if err != nil {
				continue
			}

			if len(record) < 6 {
				continue
			}

			content, err := Serialize(record[0], record[1], record[2], record[3], record[4], record[5])
			if err != nil {
				continue
			}

			statusChan := make(chan bool)
			go func() {
				statusChan <- charge(content, throttleTime, config)
			}()
			status := <-statusChan

			if !status {
				if throttleTime < maxThrottle {
					throttleTime++
				}
			} else {
				if throttleTime > 0 {
					throttleTime--
				}
			}
			Summarize(content, context, iteration, status)
			iteration++
		}
		fmt.Println("done!")
		printSummary(context, config.Application.Currency)
	}
	return nil
}
