package main

import (
	"fmt"
	"testing"

	r "github.com/stretchr/testify/require"
)

func TestSerialize_WorksCorrectly(t *testing.T) {
	result, err := Serialize("name", "100", "12345678890", "123", "10", "2021")
	if err != nil {
		fmt.Println(err)
	}
	r.Equal(t, result.Amount, int64(100))
	r.Equal(t, result.Ccnumber, "12345678890")
	r.Equal(t, result.Cvv, "123")
	r.Equal(t, result.Expmonth, 10)
	r.Equal(t, result.Expyear, 2021)
}
