module gitlab.com/abhilashr/songphapa

go 1.16

require (
	github.com/omise/omise-go v1.0.6 // indirect
	github.com/stretchr/testify v1.7.0
	gopkg.in/yaml.v2 v2.4.0
)
