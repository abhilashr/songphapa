package main

import (
	"testing"

	r "github.com/stretchr/testify/require"
	"gitlab.com/abhilashr/songphapa/model"
)

func TestSummarize_SortsCorrectly_Top(t *testing.T) {
	context := new(model.SummaryContext)
	context.Persons = make([]model.SummaryPersonContext, 3)
	context.Persons[0].Amount = 200
	context.Persons[1].Amount = 50
	context.Persons[2].Amount = 30

	record := model.Record{Name: "name", Amount: int64(1000)}
	Summarize(record, context, 1, true)

	r.Equal(t, context.Persons[0].Amount, int64(1000))
	r.Equal(t, context.Persons[1].Amount, int64(200))
	r.Equal(t, context.Persons[2].Amount, int64(50))
}

func TestSummarize_SortsCorrectly_Middle(t *testing.T) {
	context := new(model.SummaryContext)
	context.Persons = make([]model.SummaryPersonContext, 3)
	context.Persons[0].Amount = 200
	context.Persons[1].Amount = 50
	context.Persons[2].Amount = 30

	record := model.Record{Name: "name", Amount: int64(100)}
	Summarize(record, context, 1, true)

	r.Equal(t, context.Persons[0].Amount, int64(200))
	r.Equal(t, context.Persons[1].Amount, int64(100))
	r.Equal(t, context.Persons[2].Amount, int64(50))
}

func TestSummarize_SortsCorrectly_Bottom(t *testing.T) {
	context := new(model.SummaryContext)
	context.Persons = make([]model.SummaryPersonContext, 3)
	context.Persons[0].Amount = 200
	context.Persons[1].Amount = 50
	context.Persons[2].Amount = 30

	record := model.Record{Name: "name", Amount: int64(40)}
	Summarize(record, context, 1, true)

	r.Equal(t, context.Persons[0].Amount, int64(200))
	r.Equal(t, context.Persons[1].Amount, int64(50))
	r.Equal(t, context.Persons[2].Amount, int64(40))
}

func TestSummarize_StreamingAverage(t *testing.T) {
	context := new(model.SummaryContext)
	context.Persons = make([]model.SummaryPersonContext, 3)
	context.Average = 100

	record := model.Record{Name: "name", Amount: int64(200)}
	Summarize(record, context, 1, true)

	r.Equal(t, context.Average, float64(150))
}

func TestSummarize_Total(t *testing.T) {
	context := new(model.SummaryContext)
	context.Persons = make([]model.SummaryPersonContext, 3)
	context.Total = 100

	record := model.Record{Name: "name", Amount: int64(200)}
	Summarize(record, context, 1, true)

	r.Equal(t, context.Total, int64(300))
}

func TestSummarize_SuccessFailure(t *testing.T) {
	context := new(model.SummaryContext)
	context.Persons = make([]model.SummaryPersonContext, 3)

	record := model.Record{Name: "name", Amount: int64(100)}
	Summarize(record, context, 1, true)
	Summarize(record, context, 2, true)
	Summarize(record, context, 3, false)
	Summarize(record, context, 3, true)

	r.Equal(t, context.Success, int64(300))
	r.Equal(t, context.Failure, int64(100))
}
