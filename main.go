package main

import (
	"fmt"
	"os"
)

const configPath = "./config.yml"

func main() {

	args := os.Args

	if len(args) < 2 {
		fmt.Println("Expected path to csv file")
		return
	}

	config, err := NewConfig(configPath)
	if err != nil {
		fmt.Println("Unable to initialize application", err)
		return
	}

	path := os.Args[1]
	errs := process(path, config)
	if errs != nil {
		fmt.Println("Unable to perform donations :( ", err)
		return
	}
}
