package model

type Record struct {
	Name     string
	Amount   int64
	Ccnumber string
	Cvv      string
	Expmonth int
	Expyear  int
}
