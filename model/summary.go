package model

type SummaryPersonContext struct {
	Name   string
	Amount int64
}

type SummaryContext struct {
	Persons []SummaryPersonContext
	Success int64
	Failure int64
	Total   int64
	Average float64
}
